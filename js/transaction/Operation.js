var Operation = function(type) {
	this.setType(type);
};

Operation.prototype = {
	'setType': function(type) {
		if (this._type != undefined) {
			throw new AppException('Tipo de operação já definido');
		} else if (Object.values(Operation.TYPES).indexOf(type) == -1) {
			throw new AppException('Tipo de operação inválido. Valores permitidos: ' + Object.getOwnPropertyNames(Operation.TYPES).join(', '));
		}

		this._type = type;
	},

	'getType': function() {
		return this._type;
	},

	'setTransaction': function(transaction) {
		if (this._transaction != undefined) {
			throw new AppException('Transação de operação já definida');
		}

		this._transaction = transaction;
	},

	'getTransaction': function() {
		return this._transaction;
	},

	'execute': function() {
		return this.translate() + ' ' + this.getTransaction().getId();
	},

	'undo': function() {
		return 'UNDO ' + this.execute();
	},

	'translate': function() {
		var types = Object.getOwnPropertyNames(Operation.TYPES);

		return types[this.getType()];
	}
};

Object.defineProperties(Operation, {
	'TYPES': {
		'value': Object.freeze({
			'START': 0,
			'COMMIT': 1,
			'READ': 2,
			'WRITE': 3
		})
	}
});