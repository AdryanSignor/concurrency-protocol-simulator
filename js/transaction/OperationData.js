var OperationData = function(type, data) {
	Operation.apply(this, arguments);
	this.setData(data);
};

OperationData.prototype = Object.create(Operation.prototype);
OperationData.prototype._data = null;
OperationData.prototype.execute = function() {
	return this.getTransaction().getId() + ' ' + this.translate().toLowerCase() + '(' + this.getData().getId() + ')';
};

OperationData.prototype.setData = function(data) {
	if (this._data != undefined) {
		throw new AppException('Dado de operação já definido: ' + this._data.getId());
	}

	this._data = data;
};

OperationData.prototype.getData = function() {
	return this._data;
};