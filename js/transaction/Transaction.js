var Transaction = function(id, operations) {
	this.setId(id);
	this.setOperations(operations);

	var self = this;
	this.getOperations().forEach(function(operation) {
		operation.setTransaction(self);
	});

	this.rewindOperations();
};

Transaction.prototype = {
	'setId': function(id) {
		if (this._id != undefined) {
			throw new AppException('Identificador de transção já definido: ' + this._id);
		}

		this._id = id;
	},

	'getId': function() {
		return this._id;
	},

	'setTimestamp': function(timestamp) {
		if (this._timestamp != undefined) {
			throw new AppException('Timestamp de transção já definido: ' + this._timestamp);
		}

		this._timestamp = timestamp;
	},

	'getTimestamp': function() {
		return this._timestamp;
	},

	'rewindOperations': function() {
		this._operationPointer = 0;
	},

	'getNextOperation': function() {
		return this._operations[this._operationPointer++];
	},

	'setOperations': function(operations) {
		if (this._operations != undefined) {
			throw new AppException('Operações de transção já definidas');
		}

		this._operations = operations;
	},

	'getOperations': function() {
		return this._operations;
	},

	'resetTimestamp': function() {
		this._timestamp = undefined;
	}
};