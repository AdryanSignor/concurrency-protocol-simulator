var app;
$(function() {
	app = new App();
});

var App = function() {
	this.setStyles();
	this.registerEvents();
	this._operationCount = 0;
	this.clearData();

	this._timestampProtocol = new TimestampProtocol();
};

App.prototype = {
	'setStyles': function() {
		this._styles = {
			'iconAdd': 'fas fa-plus',
			'iconRemove': 'fas fa-trash-alt',
			'iconHistorySeparator': 'fas fa-arrow-circle-right',
			'buttonAdd': 'form-control btn btn-primary',
			'buttonRemove': 'form-control btn btn-danger',
			'col1': 'form-group form-control-sm col-2 col-sm-2 col-md-2 col-lg-1 col-xl-1',
			'col2': 'form-group form-control-sm col-4 col-sm-4 col-md-4 col-lg-2 col-xl-2'
		};
	},

	'registerEvents': function() {
		var self = this;
		$(document).on('click', '[data-action="navigate"]', function(event) {
			self.navigate($(event.target).attr('href'));
		}).on('click', '[data-action="addData"]', function() {
			self.addData();
		}).on('click', '[data-action="deleteData"]', function() {
			self.deleteData($(this).closest('[data-id^="data"]').attr('data-id'));
		}).on('click', '[data-action="addTransaction"]', function() {
			self.addTransaction();
		}).on('click', '[data-action="deleteTransaction"]', function() {
			self.deleteTransaction($(this).closest('[data-id^="transaction"]').attr('data-id'));
		}).on('click', '[data-action="addOperation"]', function() {
			self.addOperation($(this).closest('[data-id^="transaction"]').attr('data-id'));
		}).on('click', '[data-action="deleteOperation"]', function() {
			self.deleteOperation($(this).closest('[data-id^="operation"]').attr('data-id'), $(this).closest('[data-id^="transaction"]').attr('data-id'));
		}).on('click', '[data-action="clearExecution"]', function() {
			self.clearExecution();
		}).on('click', '[data-action="run"]', function() {
			self.run();
		}).on('hide.bs.tab', 'a[data-toggle="pill"]', function(event) {
			self.changeTabEvent(event);
		});
	},

	'clearData': function() {
		$('#data').empty();

		this._dataCount = 1;
		['x', 'y', 'z', ''].forEach(function(data) {
			this.addData(data);
		}.bind(this));
	},

	'clearTransactions': function() {
		$('#transactions').empty();

		this._transactionsCount = 1;

		for (var i = 0; i < 3; i++) {
			var transactionId = this.addTransaction();

			for (var j = 0; j < 3; j++) {
				this.addOperation(transactionId);
			}
		}
	},

	'clearExecution': function() {
		$('[data-control="hideShow"]').hide();

		$('#log').empty();
		$('#history_executed').empty();
		$('#history').empty().append(
			this.renderHistory(this._timestampProtocol.generateHistory().map(function(operation) {
				return {'text': operation.execute(), 'success': true};
			}))
		);
	},

	'loadData': function() {
		var data = [];
		$('[data-control="data"]').each(function() {
			data.push($(this).val());
		});

		this._timestampProtocol.loadData(data);

		this._optionOperations = this._timestampProtocol.getData().map(function(data) {
			return $('<option>', {'data-operation-type': 'READ', 'data-operation-data': data.getId(), 'text': 'read(' + data.getId() + ')'});
		}).concat(this._timestampProtocol.getData().map(function(data) {
			return $('<option>', {'data-operation-type': 'WRITE', 'data-operation-data': data.getId(), 'text': 'write(' + data.getId() + ')'});
		}));
	},

	'loadTransactions': function() {
		var transactions = [];

		$('[data-id^="transaction"]').each(function(i, transaction) {
			var operations = [];
			$(transaction).find('[data-control="operation"]').each(function(j, operation) {
				var selectedOption = $(operation).find(':selected');

				operations.push({
					'type': selectedOption.attr('data-operation-type'),
					'data': selectedOption.attr('data-operation-data')
				});
			});

			transactions.push({
				'id': $(transaction).find('[data-control="transaction"]').val(),
				'operations': operations
			});
		});

		this._timestampProtocol.loadTransactions(transactions);
	},

	'addData': function(value) {
		value = (value || '');

		var dataId = 'data' + this._dataCount++;
		var input = $('<input>', {'data-control': 'data', 'type': 'text', 'class': 'form-control', 'value': value});

		$('#data').append(
			$('<div>', {'data-id': dataId, 'class': this._styles.col1}).append(
				input
			),
			$('<div>', {'data-id': dataId, 'class': this._styles.col1}).append(
				$('<button>', {'data-action': 'deleteData', 'class': 'text', 'class': this._styles.buttonRemove, 'title': 'Remover dado'}).append(
					$('<i>', {'class': this._styles.iconRemove})
				)
			)
		);

		input.focus();
	},

	'deleteData': function(dataId) {
		this.deleteElement(dataId);

		if (!$('[data-id^="data"] > input').length) {
			this.addData();
		}
	},

	'addTransaction': function() {
		var transactionId = 'transaction' + this._transactionsCount;

		$('#transactions').append(
			$('<div>', {'data-id': transactionId, 'class': 'form-row mt-3'}).append(
				$('<div>', {'class': 'col-12'}).append(
					$('<div>', {'class': 'form-row'}).append(
						$('<div>', {'class': this._styles.col1}).append(
							$('<input>', {'class': 'form-control', 'type': 'text', 'data-control': 'transaction', 'value': 'T' + this._transactionsCount++})
						),
						$('<div>', {'class': this._styles.col1}).append(
							$('<button>', {'data-action': 'addOperation', 'class': this._styles.buttonAdd, 'title': 'Adicionar operação'}).append(
								$('<i>', {'class': this._styles.iconAdd})
							)
						),
						$('<div>', {'class': this._styles.col1}).append(
							$('<button>', {'data-action': 'deleteTransaction', 'class': this._styles.buttonRemove, 'title': 'Remover transação'}).append(
								$('<i>', {'class': this._styles.iconRemove})
							)
						)
					),
					$('<div>', {'class': 'form-row'})
				)
			)
		);

		this.addOperation(transactionId);

		return transactionId;
	},

	'deleteTransaction': function(transactionId) {
		this.deleteElement(transactionId);

		if (!$('[data-id^="transaction"]').length) {
			this.addTransaction();
		}
	},

	'addOperation': function(transactionId) {
		var operationId = 'operation' + this._operationCount++;
		var selectedOptionIndex = Random.generate(this._optionOperations.length);

		$('[data-id="' + transactionId + '"] > div > div').last().append(
			$('<div>', {'data-id': operationId, 'class': this._styles.col2}).append(
				$('<select>', {'data-control': 'operation', 'class': 'form-control'}).append(
					this._optionOperations.map(function(option, index) {
						return option.clone().prop('selected', selectedOptionIndex == index);
					})
				)
			),
			$('<div>', {'data-id': operationId, 'class': this._styles.col1}).append(
				$('<button>', {'data-action': 'deleteOperation', 'class': this._styles.buttonRemove, 'title': 'Remover operação'}).append(
					$('<i>', {'class': this._styles.iconRemove})
				)
			)
		);
	},

	'deleteOperation': function(operationId, transactionId) {
		this.deleteElement(operationId);

		if (!$('[data-id^="' + transactionId + '"] > div > div').last().children().length) {
			this.addOperation(transactionId);
		}
	},

	'deleteElement': function(id) {
		$('[data-id="' + id + '"]').remove();
	},

	'run': function() {
		try {
			var result = this._timestampProtocol.run();

			$('[data-control="hideShow"]').show();

			$('#log').empty().append(
				this.renderHistory(result.log)
			);

			$('#history_executed').empty().append(
				this.renderHistory(result.historyExecuted)
			);
		} catch (e) {
			this.showError(e);
		}
	},

	'renderHistory': function(historyLog) {
		return historyLog.map(function(log) {
			return [
				$('<div>', {'class': 'col-6 col-sm-6 col-md-2 col-lg-2 col-xl-2 text-center'}).append(
					$('<span>', {'text': log.text, 'class': 'badge badge-' + (log.success ? 'success' : 'danger')})
				), $('<div>', {'class': 'col-6 col-sm-6 col-md-2 col-lg-1 col-xl-1 text-center'}).append(
					$('<i>', {'class': this._styles.iconHistorySeparator})
				)
			];
		}.bind(this)).reduce(function(arrayElements, elements) {
			return arrayElements.concat(elements);
		}, []).slice(0, -1);
	},

	'showError': function(e) {
		$('#errorMsg').text(e instanceof AppException ? e.getMessage() : 'Erro inesperado');
		$('#errorDialog').modal();

		if (!(e instanceof AppException)) {
			console.error(e);
		}
	},

	'changeTabEvent': function(event) {
		try {
			if ($(event.target).attr('data-aria-controls') == 'pane_data') {
				this.loadData();
				this.clearTransactions();
			}

			if ($(event.relatedTarget).attr('data-aria-controls') == 'pane_execution') {
				this.loadTransactions();
				this.clearExecution();
			}
		} catch (e) {
			this.showError(e);
			event.preventDefault();
		}
	},

	'navigate': function(location) {
		$('[data-control="hide"]').hide();
		$(location).show();
	}
};