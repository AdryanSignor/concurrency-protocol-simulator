var TimestampProtocol = function() {
	this._globalTimestamp = new Date().getTime();
	this._transactions = [];
	this._history = [];
};

TimestampProtocol.prototype = {
	'now': function() {
		return this._globalTimestamp++;
	},

	'getData': function() {
		return this._data;
	},

	'getTransactions': function() {
		return this._transactions;
	},

	'getHistory': function() {
		return this._history;
	},

	'loadData': function(data) {
		this._data = [];

		data = data.reduce(function(parsedData, val) {
			val = val.trim();
			if (val.trim() != '') {
				parsedData.push(val);
			}

			return parsedData;
		}, []);

		if (!data.length) {
			throw new AppException('Não há dados para avançar');
		}

		var dataDuplicate = this.testDuplicated(data);

		if (dataDuplicate.length) {
			throw new AppException('Há dados duplicados: "' + dataDuplicate.join('", "') + '"');
		}

		var self = this;
		data.forEach(function(data) {
			self._data.push(new Data(data));
		});
	},

	'loadTransactions': function(transactions) {
		this._totalOperations = 0;
		this._transactions = [];

		var transactionWithoutIdCount = 0;

		var transactionsDuplicate = this.testDuplicated(transactions.map(function(transaction) {
			if (transaction.id.trim() == '') {
				transactionWithoutIdCount++;
			}

			return transaction.id.trim();
		}));

		if (transactionWithoutIdCount) {
			throw new AppException('Há ' + transactionWithoutIdCount + ' transaç' + (transactionWithoutIdCount > 1 ? 'ões' : 'ão') + ' sem identificação');
		}

		if (transactionsDuplicate.length) {
			throw new AppException('Há transações duplicadas: "' + transactionsDuplicate.join('", "') + '"');
		}

		var self = this;
		transactions.forEach(function(transaction) {
			var operations = [];

			operations.push(new Operation(Operation.TYPES.START));
			transaction.operations.forEach(function(operation) {
				var data = self._data.find(function(data) {
					if (data.getId() == operation.data) {
						return true;
					}
				});

				operations.push(new OperationData(Operation.TYPES[operation.type], data));
			});
			operations.push(new Operation(Operation.TYPES.COMMIT));

			self._transactions.push(new Transaction(transaction.id.trim(), operations));
			self._totalOperations += operations.length;
		});
	},

	'generateHistory': function() {
		this._history = [];

		while (this._history.length < this._totalOperations) {
			var operation = this._transactions[Random.generate(this._transactions.length)].getNextOperation();
			if (operation) {
				this._history.push(operation);
			}
		}

		this._transactions.forEach(function(transaction) {
			transaction.rewindOperations();
		});

		return this._history;
	},

	'run': function() {
		var originalHistory = this._history.slice();
		var log = [];

		for (var i = 0; i < this._history.length; i++) {
			var operation = this._history[i];
			var transaction = operation.getTransaction();

			if (operation.getType() == Operation.TYPES.START) {
				transaction.setTimestamp(this.now());
			} else if (operation.getType() == Operation.TYPES.READ) {
				if (transaction.getTimestamp() < operation.getData().getWriteTimestamp()) {
					i = this.abortTransaction(log, transaction, i);
					continue;
				} else {
					operation.getData().setReadTimestamp(transaction.getTimestamp());
				}
			} else if (operation.getType() == Operation.TYPES.WRITE) {
				if (transaction.getTimestamp() < operation.getData().getReadTimestamp() || transaction.getTimestamp() < operation.getData().getWriteTimestamp()) {
					i = this.abortTransaction(log, transaction, i);
					continue;
				} else if (transaction.getTimestamp() > operation.getData().getWriteTimestamp()) {
					operation.getData().setWriteTimestamp(transaction.getTimestamp());
				}
			} else {
				transaction.resetTimestamp();
			}

			log.push({'text': operation.execute(), 'success': true});
		}

		var historyExecuted = this._history.map(function(operation) {
			return {'text': operation.execute(), 'success': true};
		});
		this._history = originalHistory;

		return {
			'historyExecuted': historyExecuted,
			'log': log
		};
	},

	'abortTransaction': function(log, transaction, i) {
		log.push({'text': 'ABORT ' + transaction.getId(), 'success': false});
		for (var j = i; j > -1; j--) {
			if (this._history[j].getTransaction().getId() == transaction.getId()) {
				var operation = this._history.splice(j, 1).pop();

				if (operation.getType() == Operation.TYPES.START) {
					operation.getTransaction().resetTimestamp();
				}

				log.push({'text': operation.undo(), 'success': false});

				i--;
			}
		}

		for (var j = i; j < this._history.length; j++) {
			if (this._history[j].getTransaction().getId() == transaction.getId()) {
				this._history.splice(j, 1);
			}
		}

		this._history = this._history.concat(transaction.getOperations());

		return i;
	},

	'testDuplicated': function(array) {
		return array.reduce(function(duplicated, val, i, data) {
			if (data.indexOf(val) !== i && duplicated.indexOf(val) < 0) {
				duplicated.push(val);
			}

			return duplicated;
		}, []);
	}
};