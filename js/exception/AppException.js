var AppException = function(message) {
	Error.apply(this, arguments);
	this.message = message;
	this.stack = new Error(message).stack;
};

AppException.prototype = Object.create(Error.prototype);

AppException.prototype.getMessage = function() {
	return this.message;
};