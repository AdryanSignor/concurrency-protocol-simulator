var Data = function(id) {
	this._id = id;
	this.setReadTimestamp(0);
	this.setWriteTimestamp(0);
};

Data.prototype = {
	'getId': function() {
		return this._id;
	},

	'setReadTimestamp': function(timestamp) {
		return this._readTimestamp = timestamp;
	},

	'setWriteTimestamp': function(timestamp) {
		return this._writeTimestamp = timestamp;
	},

	'getReadTimestamp': function() {
		return this._readTimestamp;
	},

	'getWriteTimestamp': function() {
		return this._writeTimestamp;
	}
};